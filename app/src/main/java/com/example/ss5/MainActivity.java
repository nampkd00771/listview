package com.example.ss5;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private ListView lvContact;
    private List<ContactModel> listContacts = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initData();
        lvContact = (ListView) findViewById(R.id.lvContact);
        ContactAdapter adapter = new  ContactAdapter (listContacts,this);
        lvContact.setAdapter(adapter);
        lvContact.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });

    }

    private void initData() {
        ContactModel contact = new ContactModel("Nhiếp Phong", "031231231221",R.drawable.ic_male);
        listContacts.add(contact);
        contact = new ContactModel("Bộ Kinh Vân", "031231254331",R.drawable.ic_male);
        listContacts.add(contact);
        contact = new ContactModel("Đông Phương Linh Phụng", "01523523342",R.drawable.ic_female);
        listContacts.add(contact);
        contact = new ContactModel("Đông Phương Chân Long", "031231581221",R.drawable.ic_male);
        listContacts.add(contact);
        contact = new ContactModel("Thần Võ Anh Kiệt", "031231481221",R.drawable.ic_male);
        listContacts.add(contact);
        contact = new ContactModel("Hỏa Vân Tà Thần", "031231231999",R.drawable.ic_male);
        listContacts.add(contact);

    }
}
